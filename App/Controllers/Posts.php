<?php

namespace App\Controllers;

use Core\View;
use Core\Auth\Authenticated;
use App\Models\Post;


class Posts extends Authenticated
{
	
	
	protected function indexAction()
	{
		
		$posts = Post::getAll();
		
		View::renderTemplate( 'Posts/index.html', ['posts' => $posts ]);
		
	}
	
	
	protected function addNewAction()
	{
		echo get_class( $this ) . " -> <font size='+2'>addNew()</font> method.";
	}
	
	
	protected function editAction()
	{
		echo get_class( $this ) . " -> <font size='+2'>edit()</font> method.";
	}
	
}