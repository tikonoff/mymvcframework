<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;


class Home extends Controller
{
	
	protected function indexAction()
	{
		$time = date( 'Y-m-d H:i:s',time() );
		View::renderTemplate('Home/index.html', ['time' => $time] );
	}

	
	protected function before()
	{
	}
	
	
	protected function after()
	{
	}
	
	
}