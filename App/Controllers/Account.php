<?php

namespace App\Controllers;

use App\Models\User;
use Core\Controller;


class Account extends Controller
{
	
	public function validateEmailAction()
	{
		$isValid = ! User::emailExists( $_GET['email'], $_GET['ignore_id'] ?? NULL );
		
		header('Content-Type: application/json');
		echo json_encode($isValid);
	}

}