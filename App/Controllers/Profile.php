<?php
/**
 * Created by PhpStorm.
 * User: at
 * Date: 5/09/17
 * Time: 1:46 PM
 */

namespace App\Controllers;


use Core\Auth\Auth;
use Core\Auth\Authenticated;
use Core\Flash;
use Core\View;

class Profile extends Authenticated
{
	
	public function before()
	{
		$this->user = Auth::getUser();
		$this->requireLogin();
	}
	
	
	public function indexAction()
	{
		View::renderTemplate('/Profile/index.html', [ 'user' => $this->user ]);
	}

	
	public function editAction()
	{
		View::renderTemplate('/Profile/edit.html', [ 'user' => $this->user ]);
	}
	
	
	public function updateAction()
	{
		$user = $this->user;
		
		if( $user->updateProfile($_POST) ){
			Flash::addMessage('Profile updated', 'success');
			$this->redirect('/profile');
			exit;
		} else {
			Flash::addMessage( 'Something goes wrong, try again', 'warning' );
			View::renderTemplate( '/profile/edit', [ 'user' => $user ] );
		}
		
	}
	
	
}