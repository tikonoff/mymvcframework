<?php

namespace App\Models;

use Core\Model;
use PDO;

class Post extends Model
{
	
	public static function getAll()
	{
		
		try {
			$db = self::getDB();
			$stmt = $db->query('select id, title, content from posts order by created_at');
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
		
	}
}
