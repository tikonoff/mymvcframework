<?php

namespace App;


class Config
{
	
	public $dev_mode = true;     //  different datasets and error messaging

	
	
	// Common constants
	
	const SITENAME = 'MVC framework';
	const RELOGIN_DAYS =  '30';
	const SECRET_KEY = 'E31Sz1Do2aBmRxGyGexk1hZwBiQHXzzP';
	
	const MAILGUN_KEY =  'key-59d19a80a7adae89ce46868baf835213';
	const MAILGUN_DOMAIN =  'mail.redz.co.nz';
	const MAILGUN_FROM = 'RED Z <robot@redz.co.nz>';
	
	
	// Development mode constants
	
	private $local_data = [
		'DB_HOST' => 'localhost',
		'DB_NAME' => 'mvc',
		'DB_USER' => 'MVCuser',
		'DB_PASSWORD' => 'myMVCpassword',
		'SHOW_ERRORS' => true
	];
	
	
	
	// Deployment constants
	
	private $web_data = [
		'DB_HOST' => '1',
		'DB_NAME' => '1',
		'DB_USER' => '1',
		'DB_PASSWORD' => '1',
		'SECRET_KEY' => 'TIIeGHSfSYNpp6VF7kt2aI2fGPIvVbon',
		'SHOW_ERRORS' => false
	];
	
	
	
	public function init()
	{
		$initData = filter_var($this->dev_mode, FILTER_VALIDATE_BOOLEAN) === true ? $this->local_data : $this->web_data;
		
		foreach ($initData as $const => $data){
			define($const, $data);
		}
	}
	
	/**
	 * @return bool
	 */
	public function isDevMode()
	{
		return $this->dev_mode;
	}
}