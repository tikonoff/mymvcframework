<?php
/**
 * Created by PhpStorm.
 * User: at
 * Date: 17/08/17
 * Time: 1:47 AM
 */


//ini_set('session.cookie_lifetime','864000'); // ten days in seconds.


require dirname(__DIR__) . '/vendor/autoload.php';

Twig_Autoloader::register();

spl_autoload_register(function ($class) {
	$root = dirname(__DIR__);
	$file = $root . '/' . str_replace('\\','/',$class).'.php';
	if (is_readable($file)) {
		require $root.'/'.str_replace('\\','/',$class).'.php';
	}
});

error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

session_start();

$config = new \App\Config();
$config->init();

$router = new Core\Router();

$router->setRoutes('', ['controller' => 'Home', 'action' => 'index'] );
$router->setRoutes('logout', ['controller' => 'login','action' => 'logout']);
$router->setRoutes('{controller}/{action}/{token:[\da-f]+}');
$router->setRoutes('{controller}/{action}');
$router->setRoutes('{controller}',['action' => 'index']);


/*
 * If you need something like /posts/123/edit
 */

$router->setRoutes('{controller}/{id:\d+}/{action}');


$router->dispatch($_SERVER['QUERY_STRING']);


